// Require model so we could use the model for searching
const User = require("../models/user.js");
const Course = require("../models/course.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.checkEmailExist = (reqBody) => {

	// ".find" - a mongoose crud operation (query) to find a field value from a collection
	return User.find({email: reqBody.email}).then(result => {

		// condition if there is an existing user
		if(result.length >0){
			return true;
		}

		// condition if there is no existing user
		else {
			return false;
		}
	})

}

module.exports.registerUser = (reqBody) => {
	
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		/*
			// bcrypt - package for password hashing
			// hashSync - synchronously generates a hash (needs to finish before proceeding to next code)
			// hash - asynchronously generates a hash
		*/
		password: bcrypt.hashSync(reqBody.password,10),
		/*
			// 10 = salt rounds
			// salt rounds is proportional to hashing rounds, the higher the salt rounds, the more hashing rounds and the longer it takes to generate an output
			// hashing - converts a value to another value
		*/
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user, error) => {

		if(error) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
			if (result == null){
				return false;
			} else {
				// compareSynct is a bcrypt function that compares an unhashed password to a hashed password
				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

				if(isPasswordCorrect){

					return {access: auth.createAccessToken(result)};

				} else { //if passwords do not match
					return false;
					// return "Incorrect password"
				}
			}
	})
}

// s38

module.exports.getProfile = (userId) => {

	return User.findById(userId).then(result => {

		if (result == null){
			return false;
		} else {

			result.password = "******"
			return result;
		}
	})
}

// s41
module.exports.enroll = async (data) => {

	// if user is an admin, cannot enroll to a course
	if (data.isAdmin === true) {
		return false
	} else { //proceed with enrollment

		// "isUserUpdated" variable returns true upon successful enrollment, otherwise it returns false
		
		let updateUserCourses = await User.findById(data.userId).then( user => {

			// Adding the courseId in the user's enrollments array
			user.enrollments.push({courseId: data.courseId});

			// Saving the updated user information
			return user.save().then( (user, error) => {

				if(error) {
					return false
				} else {
					return true
				}
		
			});
		});

		// "updateCourseEnrollees" variable returns true upon successfull enrollment, otherwise it returns false

		let updateCourseEnrollees = await Course.findById(data.courseId).then(course => {

				// Adding the user in the course's enrollees array
				course.enrollees.push({userId: data.userId})

				// Saving the updated course information
				return course.save().then((course, error) => {

					if(error) {
						return false

					} else {
						
						return true
					}
				});

		});


		// Checking if the user and course have been updated
		if(updateUserCourses && updateCourseEnrollees == true) {

			// Enrollment successful
			return true
		} else {
			// Enrollment failed
			return false
		}
	}
}