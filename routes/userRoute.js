// dependencies
const express = require("express");

const router = express.Router();

const User = require("../models/user.js");
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

router.post("/checkEmail", (request, response) => {
	userController.checkEmailExist(request.body).then(resultFromController => response.send(resultFromController))
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/registerAdmin", (req, res) => {
	userController.registerAdmin(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

//s38
router.get("/details/:id", (req, res) => {
	userController.getProfile(req.params.id).then(resultFromController => res.send(resultFromController))
})

//s41
router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;