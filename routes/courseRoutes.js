// dependencies
const express = require("express"); // accessing package of express

const router = express.Router(); // use dot notation to access the content of a package

const courseController = require("../controllers/courseController.js");
const auth = require("../auth.js");

// CREATE A SINGLE COURSE
/*
	// [CODE FROM DISCUSSION]

	router.post("/create", (req, res) => {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	})

*/
// s39-40 Activity (Adding admin user authentication)
router.post("/create", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    courseController.addCourse(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})



// Get all courses
router.get("/all", (req, res) => {
	// courseController.getAllCourse().then(resultFromController => res.send(resultFromController)) 
	courseController.getAllCourse().then( function (resultFromController){
			res.send(resultFromController)
	})
})

// Get all ACTIVE courses
router.get("/active", (req, res) => {
	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController)) 
})

// Get a single course
router.get("/:courseId", (req, res) => {

	console.log(req.params.courseId)
	console.log(req.params)
							// retrieves the id from the url
	courseController.getCourse(req.params.courseId).then(resultFromController => res.send(resultFromController))
})

// Updating a single course
router.patch("/:courseId/update", auth.verify, (req,res) => {
	const newData = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.updateCourse(req.params.courseId, newData).then(resultFromController => {
		res.send(resultFromController)
	})
})

// s39-40 Archiving a course
router.put('/:courseId/archive', auth.verify, (req, res) => {

	console.log(req.params)

	const courseInfo = {
		courseId : req.params.courseId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	courseController.archiveCourse(courseInfo, req.body).then(resultFromController => res.send(resultFromController))
});



module.exports = router;